import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.offsetbox import OffsetImage,AnnotationBbox
from matplotlib.image import BboxImage,imread
from matplotlib.transforms import Bbox
from matplotlib.pyplot import figure



def get_house(name):
    path = f"./blasons/{name}.jpg"
    try:
        im = plt.imread(path)
    except:
        im = plt.imread("./zero.png")
    return im

def offset_image(coord, name, ax):
    img = get_house(name)
    im = OffsetImage(img, zoom=0.15)
    im.image.axes = ax

    ab = AnnotationBbox(im, (coord+0.5, 17.2),  xybox=(0., -16.), frameon=False,
                        xycoords='data',  boxcoords="offset points", pad=0)

    ax.add_artist(ab)
    ab = AnnotationBbox(im, (0.5, 17.3-coord),  xybox=(0., -16.), frameon=False,
                        xycoords='data',  boxcoords="offset points", pad=0)
    ax.add_artist(ab)
#Ordonné envers abscisse										
corresp = {
  10 : "Allié Loyal/ami",
  9 : "Bonnes Relations",
  8 : "Alliée par intéret",
  7 : "Neutalité Cordiale/Interet commun",
  6 : "Neutre",
  5 : "Méprise",
  4 : "Méfiant",
  3 : "Légérement Hostile",
  2 : "Hostilité ancienne",
  1 : "Hostile",
  0 : "Rien",
}

# Create a dataset
df = pd.read_csv("./got_relation.csv",index_col=0)

df.loc[""] = pd.Series(0, index=df.columns)
df[""]=0
df = df.sort_values(by="House",ascending=False)
df = df.sort_index(axis=1)


cmap =["#FFFFFF", "#FF0000","#FF4600","#FF7B00","#FFC100","#FFF600", "#F7FF00", "#E5FF00", "#C2FF00","#7CFF00", "#00FF00"]
houses = df.columns


figure(figsize=(32, 24), dpi=1000)
ax = sns.heatmap(df,cmap=cmap,linecolor="grey",linewidths=0.4,square=True)
ax.set_xticklabels(houses)
for i, c in enumerate(houses):
    offset_image(i, c, ax)

plt.savefig('relation_entre_maisons.svg')


